# -*- coding: utf-8 -*-

from flask import Flask, jsonify, make_response, redirect, request, render_template, url_for, Blueprint
from flask_wtf import CSRFProtect

from components.blinds import Blinds
from components.scheduler import Scheduler
from components.task import Task
from config import Config


csrf = CSRFProtect()


app = Flask(__name__)
app.config.from_object(Config())
app.static_folder = 'static'
csrf.init_app(app)
blinds = Blinds()


blinds_blueprint = Blueprint(
    'blinds',
    __name__,
    template_folder='templates/views',
    url_prefix="/blinds"
)


# -------------- cast zajistujici webovy frontend ----------------------------------------------------------------------
@app.route('/')
def index():
    return redirect(url_for('blinds.index'))


@blinds_blueprint.route('/')
def index():
    blinds_radios = blinds.get_groups()  # neni treba zjistovat http requestem
    return render_template(
        'blinds.html',
        radios=blinds_radios,
        keys=sorted(blinds_radios.keys())
    )


@blinds_blueprint.route('/settings', methods=['GET', 'POST'])
def settings():
    s = Scheduler()
    blinds_list = blinds.get_blinds()
    groups_list = blinds.get_groups()
    task_list = s.get_tasks()

    from forms.forms import BlindAddForm, GroupAddForm, TaskForm
    bform = BlindAddForm()
    gform = GroupAddForm()
    tform = TaskForm()

    return render_template(
        'settings.html',
        blinds=blinds_list,
        groups=groups_list,
        tasks=task_list,
        bform=bform,
        gform=gform,
        tform=tform
    )


@blinds_blueprint.route('/about', methods=['GET'])
def about():
    return render_template('about.html')
# -------------- cast zajistujici webovy frontend ----------------------------------------------------------------------


# -------------- API cast ----------------------------------------------------------------------------------------------
# http://127.0.0.1:5000/blinds/blind/list
@app.route('/blinds/blind/list', methods=['GET'])
def blinds_blinds_get_all():
    try:
        res = blinds.get_blinds()
        return make_response(jsonify({'status': 'ok', 'blinds': res}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/blind/add
# {
# 	"name": "test"
# }
@app.route('/blinds/blind/add', methods=['PUT'])
@csrf.exempt
def blinds_blind_add():
    data = request.get_json(force=True)
    try:
        # mistnost = data['mistnost']
        # blinds.add_room(mistnost)
        from forms.forms import BlindAddForm
        form = BlindAddForm(csrf_enabled=False)

        if not form.validate():
            return make_response(jsonify({'status': 'error', 'errors': form.errors}), 400)
        else:
            form.doform()
            res = 'Blind {} added'.format(data['name'])
            return make_response(jsonify({'status': 'ok', 'message': res, 'blind': data['name']}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/blind/remove
# {
# 	"name": "test"
# }
@app.route('/blinds/blind/remove/<name>', methods=['DELETE'])
@csrf.exempt
def blinds_blind_remove(name):
    try:
        blinds.remove(name)
        return make_response(jsonify({'status': 'ok', 'message': 'Blind {} deleted'.format(name)}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# pohyb rolety, nebude tu mistnost, ale cela skupina rolet
# http://127.0.0.1:5000/blinds/jidelna/up
@app.route('/blinds/blind/move/<group>/<smer>', methods=['POST'])
@csrf.exempt
def blinds_move(group, smer):
    # trigger = getattr(remote, smer)
    # data = request.get_json(force=True)
    try:
        # technicky tu neni blind, ale group, takze musim nacist skupinu a postupne hejbat roletou
        res = blinds.move_group(group, smer)  # data['smer']
        return make_response(jsonify({'status': 'ok', 'message': res}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# programovani rolety, na ovladaci se vybere roleta, da se prog na >3s a po kratkem pohybu rolety nahoru a dolu
# musim poslat aplikaci pozadavek na programovani rolety - musi se poslat signal na <1s
# http://127.0.0.1:5000/blinds/jidelna/up
@app.route('/blinds/blind/prog/<mistnost>', methods=['POST'])
@csrf.exempt
def blinds_prog(mistnost):
    # trigger = getattr(remote, smer)
    # data = request.get_json(force=True)
    try:
        # res = blinds.move(mistnost, data['smer'])
        res = blinds.prog(mistnost)
        return make_response(jsonify({'status': 'ok', 'message': res}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/group/add
# {
#   "id": "loznice"
# 	"nazev": "Ložnice",
# 	"mistnosti": ["loznice"]
# }
@app.route('/blinds/group/add', methods=['PUT'])
@csrf.exempt
def blinds_group_add():
    data = request.get_json(force=True)
    try:
        # neudelat to pres formular? muzu zvalidovat a vratit validacni chyby
        from forms.forms import GroupAddForm
        form = GroupAddForm(csrf_enabled=False)

        if not form.validate():
            return make_response(jsonify({'status': 'error', 'errors': form.errors}), 400)
        else:
            form.doform()
            res = 'Group {} added'.format(data['nazev'])
            return make_response(jsonify({'status': 'ok', 'message': res}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/group/list
@app.route('/blinds/group/list', methods=['GET'])
def blinds_groups_get_all():
    try:
        res = blinds.get_groups()
        return make_response(jsonify({'status': 'ok', 'groups': res}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/group/get/obyvak
@app.route('/blinds/group/get/<group>', methods=['GET'])
def blinds_groups_get_one(group):
    try:
        res = blinds.get_group(group)
        return make_response(jsonify({'status': 'ok', 'rooms': res}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/task
# [
#   {"name":"active","value":"1"},
#   {"name":"days[1]","value":"1"},
#   {"name":"days[2]","value":"1"},
#   {"name":"sunrise","value":"1"},
#   {"name":"time","value":""},
#   {"name":"blinds[]","value":"a1"},
#   {"name":"blinds[]","value":"a2"}
# ]
@app.route('/blinds/task', methods=['POST'])
def blinds_task_add():
    try:
        # res = blinds.get_group(group)
        return make_response(jsonify({'status': 'ok', 'task': None}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/task
# {
#   "id": "loznice"
# 	"nazev": "Ložnice",
# 	"mistnosti": ["loznice"]
# }
@app.route('/blinds/task', methods=['GET'])
def blinds_task_get_all():
    try:
        # res = blinds.get_group(group)
        return make_response(jsonify({'status': 'ok', 'task': None}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/task
# {
#   "id": "loznice"
# 	"nazev": "Ložnice",
# 	"mistnosti": ["loznice"]
# }
@app.route('/blinds/task/<task_id>', methods=['GET'])
def blinds_task_get_one(task_id):
    try:
        # res = blinds.get_group(group)
        return make_response(jsonify({'status': 'ok', 'task': None}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


# http://127.0.0.1:5000/blinds/task
# {
#   "id": "loznice"
# 	"nazev": "Ložnice",
# 	"mistnosti": ["loznice"]
# }
@app.route('/blinds/task/<task_id>', methods=['PUT'])
def blinds_task_update(task_id):
    try:
        # res = blinds.get_group(group)
        data = request.get_json(force=True)
        t = Task()
        return make_response(jsonify({'status': 'ok', 'task': None}))
    except Exception as error:
        return make_response(jsonify({'status': 'error', 'message': 'Error: {}'.format(repr(error))}), 400)


@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    return response
# -------------- API cast ----------------------------------------------------------------------------------------------


app.register_blueprint(blinds_blueprint)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
