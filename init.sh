if [ ! -d "venv" ]; then
    echo --------------------
    echo Creating virtualenv
    echo --------------------
    virtualenv venv
fi
source venv/bin/activate

pip install -r requirements.txt

export FLASK_APP=app.py

echo --------------------
echo Running app
echo --------------------
source venv/bin/activate; python test_data.py
