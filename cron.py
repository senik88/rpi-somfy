# -*- coding: utf-8 -*-

import argparse
from components.scheduler import Scheduler
from components.task import Task


class Cron:
    def __init__(self):
        print('init cron')


# DATE=`date +%Y-%m-%d`
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Operate Somfy Shutters.')

    parser.add_argument('--day', '-d', type=int, help='Day to run scheduler with (%s), sunday = 7')
    parser.add_argument('--hour', '-hh', type=int, help='Hour to run scheduler with (%H)')
    parser.add_argument('--minute', '-mi', type=int, help='Minute to run scheduler with (%M)')

    args = parser.parse_args()

    args_valid = True
    if args.day is None:
        print('Day invalid')
        args_valid = False

    if args.hour is None:
        print('Hour invalid')
        args_valid = False

    if args.minute is None:
        print('Minute invalid')
        args_valid = False

    if not args_valid:
        print('Use "cron.py --help" to see arguments')
        exit()

    s = Scheduler()
    for key, task in s.get_tasks().items():
        t = Task(
            active=task['active'],
            days=task['days'],
            time=task['time'],
            blinds=task['blinds'],
            description=task['description'],
            direction=task['direction']
        )

        if not t.validate(day=args.day, hour=args.hour, minute=args.minute):
            print('Task {} not valid'.format(t.description))
            continue

        t.process()

    # print(args.day, args.hour, args.minute)
