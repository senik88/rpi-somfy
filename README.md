# Rpi-Somfy #

Remote control and home automation for Somfy/Simu blinds using RPi and GPIO.
My first project in Python, it's under continuos development as I learn new things.

Consist of webapp and API, which controls data (saved blinds, groups) and movement of blinds.

Behaves as multichannel remote controller, so I'am able to controll all my blinds idividually or I can define groups to 
control several blinds together. It's also stateless controller, which means that individual commands can affect 
the ones sent previously.

In future, app will also be capable to create simple rules to control blinds automatically. For example - in working days,
blids will roll up at 7am and roll down with sunset.

## WebApp ##

## API ##

### Move blinds ###

`POST /blinds/blind/move/<mistnost>/<smer>`
Direction (smer) can be one of `up`, `down` or `stop`. Mistonst = group of blinds, one blind is also a group.

### Add new blind ###

`PUT /blinds/blind/add`
```json
{
  "mistnost": "obyvposuv"
}
```
`mistnost` is one physical blind.

### Add group of blinds ###
`PUT /blinds/group/add`
```json
{
  "id": "loznice",
  "nazev": "Ložnice",
  "mistnosti": ["loznice"]
}
```
`mistnosti` is array of physical blinds. 
This allows me to create groups I want (1st floor, 2nd floor, 1st floor without bathroom etc).

Inspired by:

*  https://github.com/Nickduino/Pi-Somfy
*  https://github.com/ViktorStiskala/rpi-blinds-controller

TODO:

*  CRUD tasks
*  view and edit blinds, groups - refactoring included
*  settings section
*  styling webapp
*  full README


INSTALACE:

-- nejde se pripojit na pigpio:
systemctl enable pigpiod
systemctl start pigpiod
service apache2 restart