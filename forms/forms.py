from components.blinds import Blinds
from components.remote import Remote
from flask_wtf import FlaskForm as Form
from wtforms import BooleanField, StringField, SelectMultipleField, widgets
from wtforms.validators import DataRequired, Length


class BlindAddForm(Form):
    name = StringField('Name (ID)', [DataRequired(), Length(max=16)])

    def doform(self):
        bl = Blinds()
        bl.add_blind(self.name.data)

        if bl.address is not None:
            remote = Remote(bl.address, bl.code)
            remote.prog(bl.name)
        else:
            raise ValueError('Blind {} was not added'.format(self.name.data))


class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()


class GroupAddForm(Form):
    nazev = StringField('Name', [DataRequired(), Length(max=24)])
    id = StringField('ID', [DataRequired(), Length(max=16)])
    mistnosti = MultiCheckboxField('Blinds', validators=[DataRequired()])  # , choices=[]

    def __init__(self, *args, **kwargs):
        super(GroupAddForm, self).__init__(*args, **kwargs)

        bl = Blinds()
        self.mistnosti.choices = [(b, b) for b in bl.get_blinds()]
        self.boxes = self.mistnosti.choices

    def doform(self):
        payload = {
            'id': self.id.data,
            'nazev': self.nazev.data,
            'mistnosti': self.mistnosti.data
        }
        bl = Blinds()
        bl.add_group(payload)


class TaskForm(Form):

    active = BooleanField('Active')

    def doform(self):
        print('doform')
