# -*- coding: utf-8 -*-

import json
import os
import random
import re

from flask import current_app
from components.remote import Remote


class Blinds:

    def __init__(self):
        self.name = None
        self.address = None
        self.code = None

    @staticmethod
    def generate_address():
        rand = '0x'
        for x in range(6):
            rand = rand + str(random.choice("0123456789ABCDEF"))
        return rand

    def add_blind(self, blind):
        path = self.get_data_path()+'/{}.txt'.format(blind)
        try:
            file = open(path, 'r')
        except FileNotFoundError:
            with open(path, 'w+') as file:
                address = self.generate_address()
                code = '0'

                file.write(address + "\n")
                file.write(code)
                file.close()

            self.address = int(address, 16)
            self.name = blind
            self.code = int(code)

            return

        raise ValueError('Blind {} already exist'.format(blind))

    def remove(self, name):
        path = self.get_data_path() + '/{}.txt'.format(name)

        # timhle osetrim ze roleta existuje
        try:
            file = open(path, 'r')
            file.close()
        except FileNotFoundError:
            raise ValueError('Blind {} does not exist'.format(name))

        os.remove(path)

        return

    def move_group(self, group, direction):
        # import time

        blinds = self.get_group(group)

        res = ''
        for blind in blinds['mistnosti']:
            res = res + "\n" + self.move(blind, direction)
            # time.sleep(50.0 / 1000.0)  # 50ms

        return res.lstrip("\n")

    def move(self, room, direction):
        path = self.get_data_path()+'/{}.txt'.format(room)
        try:
            with open(path, 'r') as file:
                data = file.readlines()
                self.address = int(data[0], 16)
                self.code = int(data[1])

                # pripravim si inkrementovany kod, ktery ulozim, ale poslu ten pred inkrementaci
                data[1] = str(self.code + 1)

                # print(self.address)
                # print(self.code)

            # tady bude nejak magie pro odeslani pres remote
            remote = Remote(self.address, self.code)
            remote.check_direction(direction)

            trigger = getattr(remote, direction)
            res = trigger(room)

            with open(path, 'w+') as file:
                file.writelines(data)

            return res
        except FileNotFoundError:
            raise ValueError('Room {} does not exist'.format(room))

    def prog(self, blind):
        path = self.get_data_path() + '/{}.txt'.format(blind)
        try:
            with open(path, 'r') as file:
                data = file.readlines()
                self.address = int(data[0], 16)
                self.code = int(data[1])  # nemel bych pri programovani poslat 0?

                # pripravim si inkrementovany kod, ktery ulozim, ale poslu ten pred inkrementaci
                data[1] = str(self.code + 1)

                # print(self.address)
                # print(self.code)
                file.close()

            # tady bude nejak magie pro odeslani pres remote
            remote = Remote(self.address, self.code)

            trigger = getattr(remote, 'prog')
            res = trigger(blind)

            with open(path, 'w+') as file:
                file.writelines(data)
                file.close()

            return res
        except FileNotFoundError:
            raise ValueError('Room {} does not exist'.format(blind))

    def add_group(self, blinds):
        path = self.get_data_path()+'/__groups.txt'
        j = self.read_groups()
        j[blinds['id']] = blinds

        with open(path, 'w+') as file:
            file.write(str(json.dumps(j, sort_keys=True)))
            file.close()

        return blinds['id']

    def get_blinds(self):
        d = self.get_data_path()
        reg = '^([a-z0-9][a-z0-9_-]+)[.]txt$'
        pattern = re.compile(reg)
        files = []
        for f in os.listdir(d):
            if pattern.match(f):
                files.append(re.match(reg, f).group(1))

        return sorted(files)

    def get_groups(self):
        return self.read_groups()

    def get_group(self, name):
        groups = self.read_groups()
        return groups[name]

    def read_groups(self):
        path = self.get_data_path()+'/__groups.txt'
        try:
            with open(path, 'r') as file:
                data = str(file.readline())
                file.close()
        except FileNotFoundError:
            data = ''

        if data == '':
            j = {}
        else:
            j = json.loads(data)

        sort = {}
        for key in sorted(j.keys()):
            sort[key] = j[key]

        return sort

    @staticmethod
    def get_data_path():
        # folder = 'dir'
        # dirname = os.path.abspath('.')
        # return os.path.join(dirname, 'data')
        return current_app.config.get('DATA_PATH')


class Blind:
    name = None
    address = None
    code = None

    def __init__(self, name, address, code=None):
        self.name = name
        self.address = address
        self.code = code
