# -*- coding: utf-8 -*-
import json

from config import Config


class Scheduler(Config):
    # def __init__(self):
    # print('scheduler', Config.DATA_PATH)

    def get_tasks(self):
        path = self.get_data_path()+'/__tasks.json'
        try:
            with open(path, 'r') as file:
                data = str(file.read())
        except FileNotFoundError:
            data = ''

        if data == '':
            j = {}
        else:
            j = json.loads(data)

        return j

    @staticmethod
    def get_data_path():
        # folder = 'dir'
        # dirname = os.path.abspath('.')
        # return os.path.join(dirname, 'data')
        return Config.DATA_PATH
