# -*- coding: utf-8 -*-

import pigpio

TXGPIO = 18  # 433.42 MHz emitter on GPIO 4 # kam je napojen data pin z RF vysilace # BCM !!!


class Remote:
    directions = ['up', 'down', 'stop']
    address = None
    code = None

    # Button values
    buttonStop = 0x1
    buttonUp__ = 0x2
    buttonDown = 0x4
    buttonProg = 0x8

    def __init__(self, address, code):
        self.address = address
        self.code = code

    def getaddress(self):
        return self.address

    def send_command(self, blind, button):
        print('{}, pressed button {}'.format(blind, button))

        # todo tohle odkomentit, pripradne to mit dle configu
        self.new_command(blind, button)

        return '{}: {} ({}), code {}'.format(blind, button, self.address, self.code)

    def up(self, blind):
        return self.send_command(blind, self.buttonUp__)

    def down(self, blind):
        return self.send_command(blind, self.buttonDown)

    def stop(self, blind):
        return self.send_command(blind, self.buttonStop)

    def prog(self, blind):
        return self.send_command(blind, self.buttonProg)

    def check_direction(self, direction):
        if not (direction in self.directions):
            raise ValueError('Invalid direction: {}'.format(direction))

    def new_command(self, blind, button, repetition=2):
        """
        todo
        je potreba spocitat, kolikrat musim poslat repetition, abych naprogramoval roletu, melo by to byt <1 s drzeni
        buttonu prog

        melo by stacit 13 opakovani pro aktivaci programovaciho modu a 2 opakovani pro potvrzeni

        Nickduino posila vzdy 2 opakovani pro pohyb rolet

        :param blind:
        :param button:
        :param repetition:
        """
        checksum = 0

        teleco = self.address
        code = self.code

        print(hex(teleco))
        print(code)

        pi = pigpio.pi()  # connect to Pi

        if not pi.connected:
            exit()

        pi.wave_add_new()
        pi.set_mode(TXGPIO, pigpio.OUTPUT)

        print("Remote  :      " + "0x%0.2X" % teleco)
        print("Button  :      " + "0x%0.2X" % button)
        print("Rolling code : " + str(code))
        print("")

        frame = bytearray(7)
        frame[0] = 0xA7  # Encryption key. Doesn't matter much
        frame[1] = button << 4  # Which button did  you press? The 4 LSB will be the checksum
        frame[2] = code >> 8  # Rolling code (big endian)
        frame[3] = (code & 0xFF)  # Rolling code
        frame[4] = teleco >> 16  # Remote address
        frame[5] = ((teleco >> 8) & 0xFF)  # Remote address
        frame[6] = (teleco & 0xFF)  # Remote address

        print("Frame  :    ")
        for octet in frame:
            print("0x%0.2X" % octet)

        print("")

        for i in range(0, 7):
            checksum = checksum ^ frame[i] ^ (frame[i] >> 4)

        checksum &= 0b1111  # We keep the last 4 bits only

        frame[1] |= checksum

        print("With cks  : ")
        for octet in frame:
            print("0x%0.2X" % octet)
        print("")

        for i in range(1, 7):
            frame[i] ^= frame[i - 1]

        print("Obfuscated :")
        for octet in frame:
            print("0x%0.2X" % octet)
        print("")

        # This is where all the awesomeness is happening. You're telling the daemon what you wanna send
        wf = []
        wf.append(pigpio.pulse(1 << TXGPIO, 0, 9415))  # wake up pulse
        wf.append(pigpio.pulse(0, 1 << TXGPIO, 89565))  # silence
        for i in range(2):  # hardware synchronization
            wf.append(pigpio.pulse(1 << TXGPIO, 0, 2560))
            wf.append(pigpio.pulse(0, 1 << TXGPIO, 2560))

        wf.append(pigpio.pulse(1 << TXGPIO, 0, 4550))  # software synchronization
        wf.append(pigpio.pulse(0, 1 << TXGPIO, 640))

        for i in range(0, 56):  # manchester enconding of payload data
            if (frame[int(i / 8)] >> (7 - (i % 8))) & 1:
                wf.append(pigpio.pulse(0, 1 << TXGPIO, 640))
                wf.append(pigpio.pulse(1 << TXGPIO, 0, 640))
            else:
                wf.append(pigpio.pulse(1 << TXGPIO, 0, 640))
                wf.append(pigpio.pulse(0, 1 << TXGPIO, 640))

        wf.append(pigpio.pulse(0, 1 << TXGPIO, 30415))  # interframe gap

        # prvni vlna je cca 0,287s , dalsi vlny jsou pak cca 0,210 s
        # z toho by se dalo vychazet pro programovani rolet
        for j in range(1, repetition):  # repeating frames
            for i in range(7):  # hardware synchronization
                wf.append(pigpio.pulse(1 << TXGPIO, 0, 2560))
                wf.append(pigpio.pulse(0, 1 << TXGPIO, 2560))

            wf.append(pigpio.pulse(1 << TXGPIO, 0, 4550))  # software synchronization
            wf.append(pigpio.pulse(0, 1 << TXGPIO, 640))

            for i in range(0, 56):  # manchester enconding of payload data
                if (frame[int(i / 8)] >> (7 - (i % 8))) & 1:
                    wf.append(pigpio.pulse(0, 1 << TXGPIO, 640))
                    wf.append(pigpio.pulse(1 << TXGPIO, 0, 640))
                else:
                    wf.append(pigpio.pulse(1 << TXGPIO, 0, 640))
                    wf.append(pigpio.pulse(0, 1 << TXGPIO, 640))

            wf.append(pigpio.pulse(0, 1 << TXGPIO, 30415))  # interframe gap

        pi.wave_add_generic(wf)
        wid = pi.wave_create()
        pi.wave_send_once(wid)
        while pi.wave_tx_busy():
            pass
        pi.wave_delete(wid)

        pi.stop()
