# from components.blinds import Blinds
# from components.remote import Remote
from datetime import datetime, timedelta
from astral import Astral


class Task:
    key = None
    description = ''
    active = True
    days = [1, 2, 3, 4, 5]
    time = '00:00'
    blinds = []
    direction = None

    def __init__(self, active=False, days=None, time=None, blinds=None, direction=None, description=''):
        """
        Konstruktor
        :param active:
        :param days:
        :param time:
        :param blinds:
        :param description:
        """
        # print('task')
        if blinds is None:
            blinds = []

        if days is None:
            days = []

        # cas muzu mit jako sunset/sunrise... a jeste si k tomu muzu chtit neco pripocitat
        reg = '^(sunset|sunrise)(([+-])([0-9]{2}):([0-9]{2}))?$'
        import re
        pattern = re.compile(reg)

        if pattern.match(time):
            m = re.search(pattern, time)
            # print(time, m.groups())

            if m.group(1) == 'sunrise':
                time = self.calc_sunrise()
            elif m.group(1) == 'sunset':
                time = self.calc_sunset()
            else:
                time = datetime.strptime(time, '%H:%M')

            if m.group(2) is not None:
                delta = timedelta(hours=int(m.group(4)), minutes=int(m.group(5)))

                if m.group(3) == '+':
                    time += delta
                elif m.group(3) == '-':
                    time -= delta

            # co kdyz to presvihne do dalsiho dne?
            time = datetime.strftime(time, '%H:%M')

        self.active = active
        self.days = days
        self.time = time
        self.blinds = blinds
        self.description = description

    def validate(self, day, hour, minute):
        """
        Validuje, jestli muze spustit
        :param day:
        :param hour:
        :param minute:
        :return:
        """
        # print(self.active, self.days, self.time)
        if not self.active:
            return False

        if day not in self.days:
            return False

        # mam hodiny, takze muzu porovnat, jestli je cas na spusteni
        time_to_run = self.time
        time_actual = '{0:02d}:{1:02d}'.format(hour, minute)
        if time_to_run != time_actual:
            return False

        return True

    def process(self):
        """
        Pokud je cas na spusteni, tak muzu task zprocesovat
        """
        if self.blinds.__len__() == 0:
            return

        # bl = Blinds()
        # for blind in self.blinds:
        #     bl.move(blind, self.direction)

        print('Task {} processed'.format(self.description))

    def calc_sunrise(self):
        return self.astral('sunrise')

    def calc_sunset(self):
        return self.astral('sunset')

    @staticmethod
    def astral(sun):
        """
        Tohle bych si mohl nekde spocitat... mit zapsany datetime, pokud bude date jine nez ulozene, tak spocitat

        :param sun:string sunrise nebo sunset
        :return:
        """
        a = Astral()
        a.solar_depression = 'civil'

        city = a['Prague']  # Praha tatim staci
        dt = city.sun(date=datetime.now(), local=True)[sun]

        # dt.strftime('%H:%M')
        return dt
