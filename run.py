import os
import re
import glob


from components.remote import Remote
from components.blinds import Blinds
import time


# ovl = Remote()
# print(ovl.up('jidelna'))
#
# time.sleep(5)
#
# print(ovl.down('jidelna'))

# blind = Blinds()
# res = blind.get_groups()
# print(res)

d = 'data'
reg = '^([a-z0-9][a-z0-9_-]+)[.]txt$'
pattern = re.compile(reg)
files = []
for f in os.listdir(d):
    if pattern.match(f):
        files.append(re.match(reg, f).group(1))

print(files)
