var Blinds;

$(document).ready(function () {

    HTMLFormElement.prototype.isValid = function() {
        return $(this).find('.form-control.is-ivalid').length == 0;
    }

    var renderAlert = function(type, message) {
        var alert = '<div class="alert alert-'+type+' alert-dismissible fade show" role="alert">'
            +message
            +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            +'<span aria-hidden="true">&times;</span>'
            +'</button>'
        +'</div>';

        $('#messages').append(alert).show();

        // za 2s alert zahodim
        setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 2000);
    };

    var validateForm = function(id, messages) {
        var control;

        // todo validace checkboxu je nejaka posahana a nefunguje
        $.each(messages, function(name, errors) {
            control = $(id+' input[name='+name+'], checkbox[name='+name+']');
            control.closest('.form-control').addClass('is-invalid');

            $.each(errors, function(k, e) {
                control.closest('.form-group').append('<div class="invalid-feedback">'+e+'</div>');
            });
        });

        window.console.log(validation);
    };

    // takhle bych mohl mit udelanych vic veci
    Blinds = (function() {
        var id = 'blinds-list-blind',
            obj = $('.'+id),
            length = obj.length
        ;

        var refresh = function() {
            if (length == 1) {
                $.ajax({
                    url: '/blinds/blind/list',
                    type: 'GET'
                }).done(function(result) {
                    window.console.log('done', result);

                    var html = '';
                    $.each(result.blinds, function(k, v) {
                        html += '<li>'+v+'</li>';
                    });

                    obj.html(html);
                }).fail(function(result) {
                    window.console.log('error', result);
                });
            }

            return this;
        }

        var remove = function(name) {
            return $.ajax({
                type: 'DELETE',
                cache: false,
                url: '/blinds/blind/remove/'+name,
                contentType: 'application/json'
            }).done(function (result) {
                renderAlert('success', result.message);
                return true;
            }).fail(function (msg) {
                renderAlert('danger', result.message);
                return false;
            });
        }

        return {
            length: length,
            refresh: refresh,
            remove: remove
        }
    })();

    $('#controls button').on('click', function() {
        $('#messages').html('');

        var move = $(this).attr('value'),
            blind = $('input[name=group]:checked').val();

        if (blind == undefined) {
            renderAlert('danger', 'No blind selected!');
            return;
        }

        // ajax call
        $.ajax({
            type: 'POST',
            cache : false,
            url: '/blinds/blind/move/'+blind+'/'+move,
            contentType: 'application/json'
        }).done(function (data) {
            window.console.log(data);
            // Obývák is going up...
            // renderAlert('success', data.message);
        }).fail(function (msg) {
            if (msg.responseJSON.message != undefined) {
                renderAlert('danger', msg.responseJSON.message);
            } else {
                renderAlert('danger', 'Unknown error!');
            }
        }).always(function (msg) {
            // console.log('ALWAYS', msg);
        });
    });

    $('#blinds-add-submit').on('click', function() {
        var button = $(this),
            formId = '#blind-add-form',
            form =  $(formId),
            formData = form.serializeArray(),
            payload = {},
            modal = $('#blinds-modal');

        $.each(formData, function(k, v) {
            payload[v.name] = v.value;
        });

        $.ajax({
            type: form.attr('data-method'),
            url: form.attr('data-action'),
            data: JSON.stringify(payload),
            contentType: "application/json"
        }).always(function(result) {
            // window.console.log(result);
            button.removeAttr("disabled");
        }).done(function (result) {
            renderAlert('success', result.message);
            modal.on('hidden.bs.modal', function() {
                form.get(0).reset();
            });

            Blinds.refresh();

            modal.modal('hide');
            // zobrazim modal s vysledkem

            var addedModal = $('#blind-added-modal');
            addedModal.find('button#blind-add-again').attr('data-blind', result.blind);
            addedModal.find('button#blind-add-abort').attr('data-blind', result.blind);
            addedModal.modal('show');
        }).fail(function (result) {
            var validation = result.responseJSON.errors;
            if (validation != undefined) {
                validateForm(formId, validation);
            }
        });
    });

    // ovladani vysledku
    $('#blind-add-again').on('click', function() {
        var blind = $(this).attr('data-blind');

        $.ajax({
            type: 'POST',
            cache : false,
            url: '/blinds/blind/prog/'+blind,
            contentType: 'application/json'
        }).done(function (data) {
            window.console.log(data);
            // renderAlert('success', data.message);
        }).fail(function (msg) {
            if (msg.responseJSON.message != undefined) {
                renderAlert('danger', msg.responseJSON.message);
            } else {
                renderAlert('danger', 'Unknown error!');
            }

            // pokud se nepovede, tak ji rovnou smazat?
        });
    });

    $('#blind-add-abort').on('click', function() {
        var blind = $(this).attr('data-blind'),
            addedModal = $('#blind-added-modal');

        $.ajax({
            type: 'DELETE',
            cache: false,
            url: '/blinds/blind/remove/'+blind,
            contentType: 'application/json'
        }).done(function (result) {
            addedModal.modal('hide');
            renderAlert('success', result.message);
        }).fail(function (msg) {
            addedModal.modal('hide');
            renderAlert('danger', result.message);
        });
//        $.when(Blinds.remove(blind), function(data) {
//            window.console.log(data);
//            addedModal.modal('hide');
//        });
    });

    $('#blind-added-modal').on('hidden.bs.modal', function() {
        $(this).find('button#blind-add-again').removeAttr('data-blind');
        $(this).find('button#blind-add-abort').removeAttr('data-blind');

        window.location.reload();
    });

    $('#group-add-submit').on('click', function() {
        $(this).attr("disabled", "disabled");

        var button = $(this),
            form =  $('#group-add-form'),
            formData = form.serializeArray(),
            payload = {},
            modal = $('#groups-modal');

        form.find('.form-control').removeClass('is-invalid');
        form.find('.form-group').find('.invalid-feedback').remove();

        $.each(formData, function(k, v) {
            if (v.name == 'mistnosti') {
                if (payload[v.name] == undefined) {
                    payload[v.name] = [];
                }
                payload[v.name].push(v.value);
            } else {
                payload[v.name] = v.value;
            }
        });

        $.ajax({
            type: form.attr('data-method'),
            url: form.attr('data-action'),
            data: JSON.stringify(payload),
            contentType: "application/json"
        }).done(function(result) {
            $.get({
                type: 'GET',
                url: '/blinds/group/list'
            }).done(function(result) {
                $('#groups-tab span.badge').text(Object.keys(result.groups).length);

                var html = '';
                $.each(result.groups, function(k, v) {
                    html += '<div><a href="#" data-group="'+k+'" data-toggle="modal">'+v.nazev+' ('+v.mistnosti.length+')</a></div>';
                });

                $('#groups-wrapper').html(html);
            });

            renderAlert('success', result.message);
            modal.on('hidden.bs.modal', function() {
                form.get(0).reset();
            });

            modal.modal('hide');
        }).fail(function(result) {
            var validation = result.responseJSON.errors;
            if (validation != undefined) {
                var control;

                // todo validace checkboxu je nejaka posahana a nefunguje
                $.each(validation, function(name, errors) {
                    control = $('#group-add-form input[name='+name+'], checkbox[name='+name+']');
                    control.closest('.form-control').addClass('is-invalid');

                    $.each(errors, function(k, e) {
                        control.closest('.form-group').append('<div class="invalid-feedback">'+e+'</div>');
                    });
                });

                window.console.log(validation);
            }
        }).always(function(result) {
            button.removeAttr("disabled");
        });
    });

    $('#task-add-submit').on('click', function() {
        var button = $(this),
            form =  $('#task-add-form'),
            formData = form.serializeArray()

        window.console.log(JSON.stringify(formData));
    })

    // nejak lepe specifikovat, co chci nacitat
    $('a.action').on('click', function(e) {
        var action = $(this).attr('data-function'),
            modal = $(this).attr('data-target'),
            group = $(this).attr('data-group'),
            task = $(this).attr('data-task'),
            f_show_modal = function(target) {
                if ($(target).length > 0) {
                    $(target).modal('show');
                }
            };

        window.console.log(action, modal, group, task);

        // nacti data, zobraz je v modalu
        if (group !== undefined) {
            $.ajax({
                url: '/blinds/group/get/'+group,
                type: 'GET',
                contentType: "application/json"
            }).done(function (result) {
                // nalit data do formulare
                var f = $(modal).find('form')[0],
                    form = $(f);

                $.each(result.rooms, function (k, v) {
                    $('input[name="'+k+'"]').val(v);
                });

                f_show_modal(modal);
            });
        } else if (task !== undefined) {

        }
    });
});