# -*- coding: utf-8 -*-

import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DATA_PATH = os.path.join(basedir, 'data')
    SECRET_KEY = "apowerfulsecretkey"
    TEMPLATES_AUTO_RELOAD = True
